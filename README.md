# Scorebird Score Data Subscription Sample App

## Prerequisites

- Node.js v15

## Installation

```
   $ npm install
```

## Usage

```
$ node scorebird-subscribe.js --nest=XXXXX --identity-pool=us-east-1:XXXXXX --endpoint=XXXXX.us-east-1.amazonaws.com

connected...

{
  device_serial: '12345',
  ts: '2022-03-07T02:56:03.312+0000',
  Quarter: '1',
  Home TOL: '5',
  Away TOL: '5',
  Home: '0',
  Away: '0',
  collected_at: '2022-03-07T02:56:06.601Z'
}

{
  device_serial: '20138',
  ts: '2022-03-07T02:52:56.394+0000',
  Time: '09:09',
  collected_at: '2022-03-07T02:52:59.677Z'
}
```

