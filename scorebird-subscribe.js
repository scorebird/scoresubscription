import CognitoIdentity from 'aws-sdk/clients/cognitoidentity.js';
import {mqtt, iot} from 'aws-iot-device-sdk-v2';
import Yargs from "yargs";

const options = Yargs(process.argv.slice(2)).option('region', {
  description: 'AWS region (supplied by Scorebird)',
  default: 'us-east-1',
  choices: ['us-east-1'],
  type: 'string'
}).option('nest', {
  description: 'Serial Number of the NeST being subscribed to',
  type: 'number'
}).option('identity-pool', {
  description: 'ID of the pool being used for auth (supplied by Scorebird)',
  type: 'string'
}).option('endpoint', {
  description: 'AWS endpoint (supplied by Scorebird)',
  type: 'string'
}).help().argv;

const region = options.region;
const nestID = options.nest;
const identityPoolId = options.identityPool;
const endpoint = options.endpoint;

const ci = new CognitoIdentity({region: region});

ci.getId( { IdentityPoolId: identityPoolId},  (err, data) => {
  ci.getCredentialsForIdentity({IdentityId: data.IdentityId}, async (err, data) => {
    const credentials = data.Credentials;
    const connection = await connect(credentials);
    await subscribe(connection);
  })
});

async function subscribe(connection) {
  await connection.subscribe(`NESTv2/${nestID}/scoreboard`, mqtt.QoS.AtLeastOnce, (topic, payload) => {
    const decoder = new TextDecoder('utf8');
    const json = decoder.decode(payload);
    const score_data = JSON.parse(json);
    //
    // additional score processing could go here...
    //
    console.log(score_data);
  });
}

function buildConfig(credentials) {
  const config_builder = iot.AwsIotMqttConnectionConfigBuilder.new_with_websockets();
  config_builder.with_credentials(region, credentials.AccessKeyId, credentials.SecretKey, credentials.SessionToken);
  config_builder.with_endpoint(endpoint);
  config_builder.with_clean_session(false);
  config_builder.with_client_id("test-" + Math.floor(Math.random() * 100000000));
  return config_builder.build();
}

function connect(credentials) {
  const config = buildConfig(credentials)
  const mqttClient = new mqtt.MqttClient();
  const connection = mqttClient.new_connection(config);

  connection.on('connect', () => {
    console.log('connected...');
  });

  connection.connect().catch(console.log);
  return connection;
}